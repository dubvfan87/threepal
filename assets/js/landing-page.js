$(function() {
  // Initialize semantic-ui widget
  $(".ui.dropdown").dropdown();

  //Make the mobile signup and login forms toggle when
  // a user clicks the buttons
  $("button#btn-mobile-signup").click(function() {
    $("div#login").hide();
    $("div#mobile-signup").fadeToggle();
  });

  $("button#btn-login").click(function() {
    $("div#mobile-signup").hide()
    $("div#login").fadeToggle();
  });

});
